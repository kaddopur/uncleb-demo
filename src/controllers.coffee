myapp = angular.module('myapp', ['ui.sortable'])

myapp.controller 'ProductController', ($scope) ->
  $scope.products = [
    {
      name:     'Mo-vitation'
      price:    '$89.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/m/o/movitation12_2.jpg'
      material: '金屬'
    },
    {
      name:     'Shift Change'
      price:    '$179.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/s/h/shiftchangegreen11a.jpg'
      material: '塑膠'
    },
    {
      name:     'Ashiru Kamui'
      price:    '$32.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/a/k/akl11_2.jpg'
      material: '塑膠'
    },
    {
      name:     'Leviathan 4β'
      price:    '$269.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/l/e/leviathan12_2.jpg'
      material: '金屬'
    },
    {
      name:     'Summit'
      price:    '$114.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/s/u/summit12_1.jpg'
      material: '金屬'
    },
    {
      name:     'Isotope 1.03'
      price:    '$269.99'
      imageUrl: 'http://www.yoyostorerewind.com/media/catalog/product/cache/3/small_image/220x220/9df78eab33525d08d6e5fb8d27136e95/i/s/isotope103_11.jpg'
      material: '金屬'
    },

  ]
  $scope.materialList = ['塑膠', '金屬']

  $scope.toggleSelect = (product, index) ->
    if $scope.targetIndex is index
      $scope.targetIndex = -1
      $scope.target = false
    else
      $scope.targetIndex = index
      $scope.target = product

  $scope.newPorduct = ->
    $scope.products.push
      name: '新產品'
      imageUrl: 'http://blog.superfuncave.com/wp-content/uploads/2012/09/ip_icon_04_New.gif'
    $scope.targetIndex = $scope.products.length-1
    $scope.target = $scope.products[$scope.targetIndex]
    

  $scope.deleteProduct = (index) ->
    $scope.products.splice(index, 1)
    $scope.targetIndex = -1
    $scope.target = false

  $scope.getClass = (index) ->
    if $scope.targetIndex is index
      "selected"
    else
      ""

